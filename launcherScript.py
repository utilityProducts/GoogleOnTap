#!/usr/bin/env python3
import os
import sys

def createJob(launcher,file,name,command):
	with open(file, "wt") as out:
		for l in launcher:
			l = l+name if l == "Name=" else l
			l = l+command if l == "Exec=" else l
			out.write(l+"\n")


if __name__ == "__main__":
	
	home = os.environ["HOME"]
	dirpath = os.path.dirname(os.path.realpath("__file__"))

	name = "GoogleOnTap"

	command = "/usr/bin/python2 {}".format(os.path.join(dirpath,"GoogleOnTap.py"))
	
	launcher = ["[Desktop Entry]", "Name=", "Exec=", "Type=Application", "X-GNOME-Autostart-enabled=true"]
	dr = home+"/.config/autostart/"
	
	if not os.path.exists(dr):
		os.makedirs(dr)
	file = dr+name.lower()+".desktop"

	if not os.path.exists(file):
		createJob(launcher,file,name,command)
	else:
		print("\nJob already exists, modifying existing task")
		os.remove(file)
		createJob(launcher,file,name,command)
		print ("Upadated !!!")